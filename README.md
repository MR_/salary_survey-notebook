# Salary Survey - Askmanager.org 

This is a Notebook based on a Salary Survey dataset from: 

https://www.askamanager.org/2021/04/how-much-money-do-you-make-4.html

## Goal

The goal of this notebook is to deal with real data which are a little bit more messy than the one usually available on Kaggle.

The answers of the survey are stored in a Google sheet, so it was used Google's API to fetch the data.

It was performed a data-cleaning and an analysis of the data touching the following topics:
* which is the most paying industry type
* whether exists a gender salary gap

## Main Contents

- Linear regression
- Permutation test
- Bootstrap test
- Blinder-Oaxaca decomposition
- Mixed linear model
- Quantile regression 

## Requirements

The requirements file contains all the packages needed to work through this notebook. In addition a R environment is needed.
